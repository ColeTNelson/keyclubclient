package k12.oostburg.cnelson.handlers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 * A multithreaded chat room server.  When a client connects the
 * server requests a screen name by sending the client the
 * text "SUBMITNAME", and keeps requesting a name until
 * a unique one is received.  After a client submits a unique
 * name, the server acknowledges with "NAMEACCEPTED".  Then
 * all messages from that client will be broadcast to all other
 * clients that have submitted a unique screen name.  The
 * broadcast messages are prefixed with "MESSAGE ".
 *
 * Because this is just a teaching example to illustrate a simple
 * chat server, there are a few features that have been left out.
 * Two are very useful and belong in production code:
 *
 *     1. The protocol should be enhanced so that the client can
 *        send clean disconnect messages to the server.
 *
 *     2. The server should do some logging.
 */
public class InputHandler implements Runnable{

    /**
     * The port that the server listens on.
     */
    private static String name;
    private static Socket socket;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;
    private static ArrayList<Object> output;

    /**
     * Constructs a handler thread, squirreling away the socket.
     * All the interesting work is done in the run method.
     */

    /**
     * Services this thread's client by repeatedly requesting a
     * screen name until a unique one has been submitted, then
     * acknowledges the name and registers the output stream for
     * the client in a global set, then repeatedly gets inputs and
     * broadcasts them.
     * @return 
     */
    
    public InputHandler(Socket socket){
    	this.socket = socket;
    }
    
    public void run() {
        try {
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
           // try {
                //socket.close();
           // } catch (IOException e) {}
        }
    }
    
    public static Object send(Object o) throws IOException, ClassNotFoundException{
    	if(in.markSupported()){
    		in.reset();
    	}
    	out.reset();
    	out.writeObject(o);
    	return in.readObject();
    }
}