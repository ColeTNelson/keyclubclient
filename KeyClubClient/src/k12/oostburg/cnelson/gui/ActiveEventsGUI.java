package k12.oostburg.cnelson.gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.Event;
import k12.oostburg.cnelson.objects.Member;

public class ActiveEventsGUI {
	
	
	private JList<String> list;
	private JButton btnAddEvent = new JButton("Add Event");
	JFrame frame;
	/**
	 * Create the application.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public ActiveEventsGUI() throws ClassNotFoundException, IOException {
		initialize();
		this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	
	private void initialize() throws ClassNotFoundException, IOException {
		btnAddEvent.setEnabled(false);
		if(((Member) InputHandler.send(Request.SELF)).isAdministrative()){
			btnAddEvent.setEnabled(true);
		}
		frame = new JFrame();
		frame.setBounds(100, 100, 338, 448);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Active Events");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 136, 29);
		frame.getContentPane().add(lblKeyClub);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 55, 408, 2);
		frame.getContentPane().add(separator);
		
		JButton btnView = new JButton("View");
		btnView.setBounds(12, 367, 76, 25);
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(list.getSelectedValue()==null){
					return;
				}
				try {
					InputHandler.send(Request.GET_EVENT);
					String event = list.getSelectedValue();
					String[] split = event.split("\\[");
					UUID uuid = UUID.fromString(split[split.length-1].split("\\]")[0]);
					Event eventee = (Event) InputHandler.send(uuid);
					new EventDetailsGUI(eventee);
				} catch (ClassNotFoundException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frame.getContentPane().add(btnView);
		JScrollPane scroll = new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		ArrayList<String> strEvents = new ArrayList<String>();
		ArrayList<Event> events;
		try {
			events = (ArrayList<Event>) InputHandler.send(Request.EVENTS);
			for(Event e : events){
				strEvents.add(e.getDate()+": "+e.getName()+"                                                                  ["+e.getUUID()+"]");
			}
			list = new JList(strEvents.toArray());
			list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
			list.setLayoutOrientation(JList.VERTICAL);
			list.setVisibleRowCount(-1);
			list.setBounds(12, 70, 297, 284);
			frame.getContentPane().add(list);
			frame.getContentPane().add(scroll);
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}
		
		btnAddEvent.setBounds(206, 367, 101, 25);
		btnAddEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Event newEvent;
				try {
					newEvent = (Event) InputHandler.send(Request.ADD_EVENT);
					new EventDetailsGUI(newEvent);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		frame.getContentPane().add(btnAddEvent);
		
		JButton btnReload = new JButton("Refresh");
		btnReload.setBounds(100, 367, 94, 25);
		btnReload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					frame.dispose();
					new ActiveEventsGUI();
				} catch (ClassNotFoundException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frame.getContentPane().add(btnReload);
		
		JButton btnArchivedEvents = new JButton("Archived Events");
		btnArchivedEvents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				try {
					frame.dispose();
					new ArchivedEventsGUI();
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnArchivedEvents.setBounds(173, 16, 136, 25);
		frame.getContentPane().add(btnArchivedEvents);
	}
}
	