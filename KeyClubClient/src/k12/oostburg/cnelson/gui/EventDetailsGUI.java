package k12.oostburg.cnelson.gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.Event;
import k12.oostburg.cnelson.objects.Member;

public class EventDetailsGUI {

	private Event e;
	private JFrame frame;
	private JTextField tfEventName;
	private JTextField tfAddress;
	private JTextField tfTime;
	private JTextField tfDate;
	private JSeparator separator_1;
	private JTextField tfHours;
	private JTextField tfMaxVolunteers;
	private JTextPane txtVolunteers = new JTextPane();
	private JTextPane txtInfo = new JTextPane();
	private JButton btnSave = new JButton("Save");
	private JButton btnDelete = new JButton("Delete");
	private JButton btnRemove = new JButton("- Me -");
	private JButton btnAdd = new JButton("+ Me +");
	private JButton btnSendReminderEmail = new JButton("Remind");
	private JButton btnAddVolunteer = new JButton("+ Volunteer");
	private JButton btnRemoveVolunteer = new JButton("- Volunteer");
	private JButton btnArchive = new JButton("Archive");
	private JButton btnUnarchive = new JButton("Unarchive");
	private JTextField tfAddVolunteer;
	private JTextField tfRemoveVolunteer;
	/**
	 * Create the application.
	 */
	public EventDetailsGUI(Event e){
		this.e = e;
		initialize();
		Member m;
		txtVolunteers.setEditable(false);
		btnSave.setEnabled(false);
		btnDelete.setEnabled(false);
		btnSendReminderEmail.setEnabled(false);
		btnAdd.setVisible(true);
		btnRemove.setVisible(false);
		btnAddVolunteer.setEnabled(false);
		btnRemoveVolunteer.setEnabled(false);
		btnArchive.setEnabled(false);
		btnUnarchive.setEnabled(false);
		try {
			m = (Member) InputHandler.send(Request.SELF);
			if(m.isAdministrative()){
				btnSave.setEnabled(true);
				btnDelete.setEnabled(true);
				btnSendReminderEmail.setEnabled(true);
				btnArchive.setEnabled(true);
				btnUnarchive.setEnabled(true);
				btnAddVolunteer.setEnabled(true);
				btnRemoveVolunteer.setEnabled(true);
			}
			UUID uuid = m.getUUID();
			for(Member member : e.getVolunteers()){
				if(member==null){
					continue;
				}
				if(uuid.equals(member.getUUID())){
					btnAdd.setVisible(false);
					btnRemove.setVisible(true);
				}
			}
			if(e.isArchived()){
				btnArchive.setVisible(false);
				btnUnarchive.setVisible(true);
				if(!m.isAdministrative()){
					btnAdd.setEnabled(false);
					btnRemove.setEnabled(false);
				}
			}else{
				btnArchive.setVisible(true);
				btnUnarchive.setVisible(false);
			}
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}
		tfEventName.setText(e.getName());
		tfDate.setText(e.getDate());
		tfTime.setText(e.getTime());
		tfAddress.setText(e.getAddress());
		tfHours.setText(String.valueOf(e.getHours()));
		tfMaxVolunteers.setText(String.valueOf(e.getMaxVolunteers()));
		String info = "";
		for(String s : e.getInfo()){
			info = info.concat(s+"\n");
		}
		txtInfo.setText(info);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					save();
					InputHandler.send(Request.UPDATE_EVENT);
					InputHandler.send(e);
					JOptionPane.showMessageDialog(new JFrame(),
						    "Event has been saved!",
						    "Key Club",
						    JOptionPane.PLAIN_MESSAGE);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnSave.setBounds(265, 510, 75, 25);
		frame.getContentPane().add(btnSave);

		btnDelete.setBounds(349, 510, 75, 25);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					InputHandler.send(Request.REMOVE_EVENT);
					InputHandler.send(e);
					frame.dispose();
					JOptionPane.showMessageDialog(new JFrame(),
						    "Event has been removed!",
						    "Key Club",
						    JOptionPane.PLAIN_MESSAGE);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		frame.getContentPane().add(btnDelete);

		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					Member self = (Member) InputHandler.send(Request.SELF);
					e.addVolunteer(self);
					InputHandler.send(Request.UPDATE_EVENT);
					InputHandler.send(e);
					updateVolunteersText();
					btnAdd.setVisible(false);
					btnRemove.setVisible(true);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAdd.setBounds(316, 48, 100, 25);
		frame.getContentPane().add(btnAdd);

		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					Member self = (Member) InputHandler.send(Request.SELF);
					e.removeVolunteer(self);
					InputHandler.send(Request.UPDATE_EVENT);
					InputHandler.send(e);
					updateVolunteersText();
					btnAdd.setVisible(true);
					btnRemove.setVisible(false);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnRemove.setBounds(316, 48, 100, 25);
		frame.getContentPane().add(btnRemove);
		
		JLabel lblInformation = new JLabel("Information");
		lblInformation.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		lblInformation.setBounds(234, 176, 106, 16);
		frame.getContentPane().add(lblInformation);
		
		txtInfo.setBounds(237, 205, 172, 206);
		frame.getContentPane().add(txtInfo);
		
		JLabel lblMaxVolunteers = new JLabel("Max Volunteers");
		lblMaxVolunteers.setFont(new Font("Georgia", Font.BOLD, 14));
		lblMaxVolunteers.setBounds(239, 149, 132, 16);
		frame.getContentPane().add(lblMaxVolunteers);
		
		tfAddVolunteer = new JTextField();
		tfAddVolunteer.setBounds(39, 439, 215, 22);
		frame.getContentPane().add(tfAddVolunteer);
		tfAddVolunteer.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(7, 424, 397, 2);
		frame.getContentPane().add(separator);
		

		btnAddVolunteer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				try {
					InputHandler.send(Request.IDENTIFY);
					Member m = (Member) InputHandler.send(tfAddVolunteer.getText());
					if(m==null){
						JOptionPane.showMessageDialog(new JFrame(),
							    "Could not find "+tfAddVolunteer.getText()+"... There may be none or more than one of these members.",
							    "Key Club",
							    JOptionPane.PLAIN_MESSAGE);
						return;
					}
					e.addVolunteer(m);
					InputHandler.send(Request.UPDATE_EVENT);
					InputHandler.send(e);
					updateVolunteersText();
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnAddVolunteer.setBounds(273, 438, 117, 25);
		frame.getContentPane().add(btnAddVolunteer);
		
		tfRemoveVolunteer = new JTextField();
		tfRemoveVolunteer.setColumns(10);
		tfRemoveVolunteer.setBounds(39, 475, 215, 22);
		frame.getContentPane().add(tfRemoveVolunteer);
		
		btnRemoveVolunteer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				try {
					InputHandler.send(Request.IDENTIFY);
					Member m = (Member) InputHandler.send(tfRemoveVolunteer.getText());
					if(m==null){
						JOptionPane.showMessageDialog(new JFrame(),
							    "Could not find "+tfRemoveVolunteer.getText()+"... There may be none or more than one of these members.",
							    "Key Club",
							    JOptionPane.PLAIN_MESSAGE);
						return;
					}
					e.removeVolunteer(m);
					InputHandler.send(Request.UPDATE_EVENT);
					InputHandler.send(e);
					updateVolunteersText();
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnRemoveVolunteer.setBounds(273, 474, 117, 25);
		frame.getContentPane().add(btnRemoveVolunteer);
		
		txtVolunteers.setText("");
		txtVolunteers.setBounds(12, 205, 205, 206);
		frame.getContentPane().add(txtVolunteers);
		
		btnArchive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				try {
					e.setArchived(true);
					btnArchive.setVisible(false);
					btnUnarchive.setVisible(true);
					InputHandler.send(Request.UPDATE_EVENT);
					InputHandler.send(e);
					InputHandler.send(Request.SWITCH_ARCHIVE);
					InputHandler.send(e);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnArchive.setBounds(114, 510, 100, 25);
		frame.getContentPane().add(btnArchive);
		
		btnUnarchive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				try {
					e.setArchived(false);
					btnArchive.setVisible(true);
					btnUnarchive.setVisible(false);
					InputHandler.send(Request.UPDATE_EVENT);
					InputHandler.send(e);
					InputHandler.send(Request.SWITCH_ARCHIVE);
					InputHandler.send(e);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnUnarchive.setBounds(114, 510, 100, 25);
		frame.getContentPane().add(btnUnarchive);
		
		btnSendReminderEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					save();
					InputHandler.send(Request.SEND_REMIND_EMAIL);
					InputHandler.send(e);
					JOptionPane.showMessageDialog(new JFrame(),
						    "Sent Reminder Email!",
						    "Key Club",
						    JOptionPane.PLAIN_MESSAGE);
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		updateVolunteersText();
		
		this.frame.setVisible(true);
	}

	protected void save() {
		e.setAddress(tfAddress.getText());
		e.setDate(tfDate.getText());
		e.setHours(Double.parseDouble(tfHours.getText()));
		ArrayList<String> info = new ArrayList<String>();
		info.add(txtInfo.getText());
		e.setInfo(info);
		e.setMaxVolunteers(Integer.parseInt(tfMaxVolunteers.getText()));
		e.setName(tfEventName.getText());
		e.setTime(tfTime.getText());
		
	}

	protected void updateVolunteersText() {
		String volunteers = "";
		for(Member volunteer : e.getVolunteers()){
			if(volunteer==null){
				continue;
			}
			volunteers = volunteers.concat(volunteer.getFullName()+"\n");
		}
		txtVolunteers.setText(volunteers);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setEnabled(false);
		frame.setBounds(100, 100, 450, 599);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCustomReminderEmail = new JLabel("Event Details");
		lblCustomReminderEmail.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCustomReminderEmail.setBounds(12, 8, 117, 23);
		frame.getContentPane().add(lblCustomReminderEmail);
		
		JLabel lblEventName = new JLabel("Event Name:");
		lblEventName.setFont(new Font("Georgia", Font.BOLD, 14));
		lblEventName.setBounds(12, 52, 106, 16);
		frame.getContentPane().add(lblEventName);
		
		tfEventName = new JTextField();
		tfEventName.setBounds(114, 49, 190, 22);
		frame.getContentPane().add(tfEventName);
		tfEventName.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Georgia", Font.BOLD, 14));
		lblAddress.setBounds(12, 84, 106, 16);
		frame.getContentPane().add(lblAddress);
		
		tfAddress = new JTextField();
		tfAddress.setColumns(10);
		tfAddress.setBounds(83, 81, 190, 22);
		frame.getContentPane().add(tfAddress);
		
		tfMaxVolunteers = new JTextField();
		tfMaxVolunteers.setBounds(368, 146, 36, 22);
		frame.getContentPane().add(tfMaxVolunteers);
		tfMaxVolunteers.setColumns(10);
		
		JLabel lblTime = new JLabel("Time:");
		lblTime.setFont(new Font("Georgia", Font.BOLD, 14));
		lblTime.setBounds(234, 119, 53, 16);
		frame.getContentPane().add(lblTime);
		
		tfTime = new JTextField();
		tfTime.setColumns(10);
		tfTime.setBounds(284, 116, 106, 22);
		frame.getContentPane().add(tfTime);
		
		JLabel lblDate = new JLabel("Date:");
		lblDate.setFont(new Font("Georgia", Font.BOLD, 14));
		lblDate.setBounds(12, 119, 59, 16);
		frame.getContentPane().add(lblDate);
		
		tfDate = new JTextField();
		tfDate.setColumns(10);
		tfDate.setBounds(58, 116, 159, 22);
		frame.getContentPane().add(tfDate);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 161, 210, 2);
		frame.getContentPane().add(separator);
		
		JLabel lblVolunteers = new JLabel("Volunteers");
		lblVolunteers.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		lblVolunteers.setBounds(12, 176, 132, 16);
		frame.getContentPane().add(lblVolunteers);
		
		separator_1 = new JSeparator();
		separator_1.setBounds(12, 40, 397, 2);
		frame.getContentPane().add(separator_1);
		
		btnSendReminderEmail.setBounds(12, 510, 89, 25);
		frame.getContentPane().add(btnSendReminderEmail);
		
		tfHours = new JTextField();
		tfHours.setBounds(349, 81, 71, 22);
		frame.getContentPane().add(tfHours);
		tfHours.setColumns(10);
		
		JLabel lblHours = new JLabel("Hours:");
		lblHours.setFont(new Font("Georgia", Font.BOLD, 14));
		lblHours.setBounds(285, 84, 59, 16);
		frame.getContentPane().add(lblHours);
	}
}
