package k12.oostburg.cnelson.gui;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.enums.ResponseCode;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.Email;
import k12.oostburg.cnelson.objects.Member;
import k12.oostburg.cnelson.statics.Encrypter;

public class ForgotPasswordGUI {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public ForgotPasswordGUI() {
		initialize();
		this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 312, 245);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Forgot Password");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 273, 29);
		frame.getContentPane().add(lblKeyClub);
		
		textField = new JTextField();
		textField.setBounds(75, 55, 198, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Email");
		lblNewLabel.setBounds(22, 58, 41, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JTextPane txtpnAn = new JTextPane();
		txtpnAn.setBackground(SystemColor.control);
		txtpnAn.setText("An email will be sent to you containing your new password. Make sure to reset this as soon as you login.");
		txtpnAn.setBounds(32, 90, 250, 57);
		frame.getContentPane().add(txtpnAn);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					String email = textField.getText();
					InputHandler.send(Request.GET_MEMBER);
					Member m = (Member) InputHandler.send(textField.getText());
					if(m==null){
						JOptionPane.showMessageDialog(new JFrame(),
							    "Invalid email address!",
							    "Key Club",
							    JOptionPane.PLAIN_MESSAGE);
						return;
					}
					InputHandler.send(Request.RESET_PASSWORD_OTHER);
					InputHandler.send(m.getUUID());
					ArrayList<String> text = new ArrayList<String>();
					text.add("[FULL_NAME]:");
					text.add("");
					text.add("You have requested a password reset. Your new password is '[PASSWORD]'");
					text.add("");
					text.add("Sincerely,");
					text.add("Oostburg Key Club");
					ArrayList<Email> emails = new ArrayList<Email>();
					emails.add(new Email(m, "Key Club - Password Reset", text));
					InputHandler.send(Request.EMAIL);
					ResponseCode rc = (ResponseCode) InputHandler.send(emails);
					if(rc.equals(ResponseCode.UNDEFINED_FAILURE)){
						JOptionPane.showMessageDialog(new JFrame(),
							    "The email server is currently under maintenance. Please try again later.",
							    "Key Club",
							    JOptionPane.WARNING_MESSAGE);
						return;
					}
					InputHandler.send(emails);
					frame.dispose();
					JOptionPane.showMessageDialog(new JFrame(),
						    "A new password has been sent to '"+email+"'",
						    "Key Club",
						    JOptionPane.PLAIN_MESSAGE);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnSubmit.setBounds(117, 160, 80, 25);
		frame.getContentPane().add(btnSubmit);
	}
}
