package k12.oostburg.cnelson.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.enums.ResponseCode;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.LoginPair;
import k12.oostburg.cnelson.objects.Member;
import k12.oostburg.cnelson.statics.Encrypter;

public class LoginGUI {

	private JFrame frmKeyClub;
	private JTextField emailField;
	private JPasswordField passwordField;
	private JLabel lblLoginFailure;

	/**
	 * Create the application.
	 */
	public LoginGUI() {
		initialize();
		this.frmKeyClub.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKeyClub = new JFrame();
		frmKeyClub.setTitle("Key Club - Login");
		frmKeyClub.setBounds(100, 100, 320, 203);
		frmKeyClub.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKeyClub.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Login");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 408, 20);
		frmKeyClub.getContentPane().add(lblKeyClub);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(12, 46, 46, 16);
		frmKeyClub.getContentPane().add(lblEmail);
		
		emailField = new JTextField();
		emailField.setBounds(90, 43, 189, 22);
		frmKeyClub.getContentPane().add(emailField);
		emailField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(12, 81, 66, 16);
		frmKeyClub.getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(90, 78, 189, 22);
		frmKeyClub.getContentPane().add(passwordField);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(12, 110, 97, 25);
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ResponseCode rc = (ResponseCode) InputHandler.send(new LoginPair(emailField.getText(), Encrypter.encrypt(passwordField.getText())));
					if(rc.equals(ResponseCode.SUCCESS)){
						frmKeyClub.dispose();
						new MenuGUI();
					}else{
						lblLoginFailure.setVisible(true);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		frmKeyClub.getContentPane().add(btnSubmit);
		
		JButton btnForgotPassword = new JButton("Forgot Password?");
		btnForgotPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ForgotPasswordGUI();
			}
		});
		btnForgotPassword.setBounds(135, 110, 144, 25);
		frmKeyClub.getContentPane().add(btnForgotPassword);
		
		lblLoginFailure = new JLabel("Login Failure!");
		lblLoginFailure.setForeground(Color.RED);
		lblLoginFailure.setBounds(156, 16, 104, 16);
		frmKeyClub.getContentPane().add(lblLoginFailure);
		
		JButton btnIp = new JButton("IP");
		btnIp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ConfigureConnectionGUI();
			}
		});
		btnIp.setBounds(244, 12, 46, 25);
		frmKeyClub.getContentPane().add(btnIp);
		lblLoginFailure.setVisible(false);
	}
}
