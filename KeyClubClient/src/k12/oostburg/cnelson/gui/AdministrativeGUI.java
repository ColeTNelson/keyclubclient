package k12.oostburg.cnelson.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.Member;

public class AdministrativeGUI {

	private JFrame frame;
	private JTextField tfSearch;
	JButton btnConfirmAction = new JButton("Confirm");

	public AdministrativeGUI() {
		initialize();
		this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		btnConfirmAction.setEnabled(false);
		frame = new JFrame();
		frame.setBounds(100, 100, 320, 221);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Administrative");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 283, 30);
		frame.getContentPane().add(lblKeyClub);
		
		JButton btnReminderEmail = new JButton("Reminder Email");
		btnReminderEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ReminderEmailGUI();
			}
		});
		btnReminderEmail.setBounds(12, 98, 124, 25);
		frame.getContentPane().add(btnReminderEmail);
		
		JButton btnCreateMember = new JButton("Create Member");
		btnCreateMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Member m = (Member) InputHandler.send(Request.ADD_MEMBER);
					new ProfileGUI(m);
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnCreateMember.setBounds(148, 98, 124, 25);
		frame.getContentPane().add(btnCreateMember);
		
		JButton btnWipe = new JButton("WIPE - NEW YEAR");
		btnWipe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnConfirmAction.setEnabled(true);
			}
		});
		btnWipe.setBounds(12, 136, 169, 25);
		frame.getContentPane().add(btnWipe);
		
		JButton btnSearch = new JButton("Search Member");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					InputHandler.send(Request.IDENTIFY);
					Member m = (Member) InputHandler.send(tfSearch.getText());
					if(m==null){
						JOptionPane.showMessageDialog(new JFrame(),
							    "Could not find "+tfSearch.getText()+"... Either none or more than one of these members exists!",
							    "Key Club",
							    JOptionPane.PLAIN_MESSAGE);
						return;
					}
					new ProfileGUI(m);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		btnSearch.setBounds(171, 60, 124, 25);
		frame.getContentPane().add(btnSearch);
		
		btnConfirmAction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//TODO Wipe
				System.out.println("Would have wiped.");
			}
		});
		btnConfirmAction.setBounds(193, 136, 89, 25);
		frame.getContentPane().add(btnConfirmAction);
		
		tfSearch = new JTextField();
		tfSearch.setBounds(12, 60, 147, 22);
		frame.getContentPane().add(tfSearch);
		tfSearch.setColumns(10);
	}
}
