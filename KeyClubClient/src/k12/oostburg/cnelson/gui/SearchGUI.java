package k12.oostburg.cnelson.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SearchGUI {

	private JFrame frmKeyClub;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchGUI window = new SearchGUI();
					window.frmKeyClub.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SearchGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKeyClub = new JFrame();
		frmKeyClub.setTitle("Key Club - Search");
		frmKeyClub.setBounds(100, 100, 450, 360);
		frmKeyClub.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKeyClub.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Search For Member");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 327, 20);
		frmKeyClub.getContentPane().add(lblKeyClub);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 46, 408, 2);
		frmKeyClub.getContentPane().add(separator);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(12, 61, 83, 16);
		frmKeyClub.getContentPane().add(lblFirstName);
		
		textField = new JTextField();
		textField.setBounds(83, 58, 83, 22);
		frmKeyClub.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(178, 61, 83, 16);
		frmKeyClub.getContentPane().add(lblLastName);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(244, 58, 83, 22);
		frmKeyClub.getContentPane().add(textField_1);
		
		JLabel lblGrade = new JLabel("Grade");
		lblGrade.setBounds(339, 61, 56, 16);
		frmKeyClub.getContentPane().add(lblGrade);
		
		textField_2 = new JTextField();
		textField_2.setBounds(380, 58, 34, 22);
		frmKeyClub.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setBounds(291, 12, 83, 25);
		frmKeyClub.getContentPane().add(btnSearch);
		
		JList list = new JList();
		list.setBounds(12, 90, 315, 195);
		frmKeyClub.getContentPane().add(list);
		
		JButton btnView = new JButton("View");
		btnView.setBounds(339, 166, 75, 25);
		frmKeyClub.getContentPane().add(btnView);
		
		JButton btnEmail = new JButton("Email");
		btnEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEmail.setBounds(339, 204, 75, 25);
		frmKeyClub.getContentPane().add(btnEmail);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(339, 151, 81, 2);
		frmKeyClub.getContentPane().add(separator_1);
		
		JLabel lblOptions = new JLabel("Options");
		lblOptions.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblOptions.setBounds(352, 121, 56, 16);
		frmKeyClub.getContentPane().add(lblOptions);
	}

}
