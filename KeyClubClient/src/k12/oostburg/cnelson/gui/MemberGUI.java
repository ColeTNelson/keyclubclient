package k12.oostburg.cnelson.gui;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;

public class MemberGUI extends JInternalFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MemberGUI frame = new MemberGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MemberGUI() {
		setBounds(100, 100, 450, 300);

	}

}
