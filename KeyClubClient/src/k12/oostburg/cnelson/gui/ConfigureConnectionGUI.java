package k12.oostburg.cnelson.gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import k12.oostburg.cnelson.statics.KeyClubClient;

public class ConfigureConnectionGUI {

	private JFrame frame;
	private JTextField ip;
	private JTextField port;

	/**
	 * Create the application.
	 */
	public ConfigureConnectionGUI() {	
		initialize();
		this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 233, 204);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIpAddress = new JLabel("IP Address");
		lblIpAddress.setBounds(12, 60, 114, 16);
		frame.getContentPane().add(lblIpAddress);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(47, 89, 56, 16);
		frame.getContentPane().add(lblPort);
		
		ip = new JTextField();
		ip.setBounds(83, 57, 116, 22);
		frame.getContentPane().add(ip);
		ip.setColumns(10);
		ip.setText(KeyClubClient.getConnectionIp());
		
		port = new JTextField();
		port.setBounds(84, 86, 116, 22);
		frame.getContentPane().add(port);
		port.setColumns(10);
		port.setText(String.valueOf(KeyClubClient.getConnectionPort()));
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					KeyClubClient.updateConnection(ip.getText(), Integer.parseInt(port.getText()));
					frame.dispose();
				} catch (NumberFormatException | IOException e) {}
			}
		});
		btnUpdate.setBounds(102, 121, 97, 25);
		frame.getContentPane().add(btnUpdate);
		
		JLabel lblConnectionConfiguration = new JLabel("Connection Configuration");
		lblConnectionConfiguration.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblConnectionConfiguration.setBounds(12, 13, 198, 34);
		frame.getContentPane().add(lblConnectionConfiguration);
	}
}
