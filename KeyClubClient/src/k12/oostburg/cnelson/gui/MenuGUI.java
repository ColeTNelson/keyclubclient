package k12.oostburg.cnelson.gui;

import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.Member;

public class MenuGUI {

	private JFrame frmKeyClub;
	private JButton btnAdmin = new JButton("Administrative");

	/**
	 * Create the application.
	 */
	public MenuGUI() {
		initialize();
		this.frmKeyClub.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		btnAdmin.setEnabled(false);
		Member m;
		try {
			m = (Member) InputHandler.send(Request.SELF);
			if(m.isAdministrative()){
				btnAdmin.setEnabled(true);
			}
		} catch (ClassNotFoundException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		frmKeyClub = new JFrame();
		frmKeyClub.setTitle("Key Club - Menu");
		frmKeyClub.setBounds(100, 100, 272, 300);
		frmKeyClub.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKeyClub.getContentPane().setLayout(null);
		
		JLabel lblKeyClub = new JLabel("Key Club - Menu");
		lblKeyClub.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblKeyClub.setBounds(12, 13, 219, 30);
		frmKeyClub.getContentPane().add(lblKeyClub);
		
		JButton btnEvents = new JButton("Events");
		btnEvents.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					new ActiveEventsGUI();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnEvents.setBounds(12, 78, 97, 25);
		frmKeyClub.getContentPane().add(btnEvents);
		
		JButton btnProfile = new JButton("Profile");
		btnProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object inc = null;
				try {
					inc = InputHandler.send(Request.SELF);
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
				if(inc instanceof Member){
					new ProfileGUI((Member) inc);
				}else{}
				
			}
		});
		btnProfile.setBounds(162, 17, 80, 25);
		frmKeyClub.getContentPane().add(btnProfile);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(12, 61, 227, 3);
		frmKeyClub.getContentPane().add(separator);
		
		JButton btnCalendar = new JButton("Calendar");
		btnCalendar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().browse(new URI("https://calendar.google.com/calendar/embed?src=oostburg.k12.wi.us_dhj2dqf8mi688hhl3vo0ls4rk0@group.calendar.google.com&ctz=America/Chicago"));
				} catch (IOException | URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnCalendar.setBounds(145, 163, 97, 25);
		frmKeyClub.getContentPane().add(btnCalendar);
		
		JButton btnWebsite = new JButton("Website");
		btnWebsite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().browse(new URI("http://oostburg.k12.wi.us/portfolio/key-club/"));
				} catch (IOException | URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnWebsite.setBounds(12, 201, 97, 25);
		frmKeyClub.getContentPane().add(btnWebsite);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 154, 232, 2);
		frmKeyClub.getContentPane().add(separator_1);
		
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AdministrativeGUI();
			}
		});
		btnAdmin.setBounds(22, 116, 209, 25);
		frmKeyClub.getContentPane().add(btnAdmin);
		
		
		JButton btnHours = new JButton("Hours");
		btnHours.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					new MyEventsGUI();
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnHours.setBounds(145, 78, 97, 25);
		frmKeyClub.getContentPane().add(btnHours);
		
		JButton btnPolicy = new JButton("Policy");
		btnPolicy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().browse(new URI("https://docs.google.com/document/d/1_CcVEhDeW5SsPmDknNbHshEAlIm6OhI6um4kWdNtmj0/"));
				} catch (IOException | URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnPolicy.setBounds(12, 163, 97, 25);
		frmKeyClub.getContentPane().add(btnPolicy);
		
		JButton btnContact = new JButton("Contact");
		btnContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(new JFrame(),
					    "Please e-mail us at oostburgkeyclub@gmail.com",
					    "Key Club",
					    JOptionPane.PLAIN_MESSAGE);
			}
		});
		btnContact.setBounds(145, 201, 97, 25);
		frmKeyClub.getContentPane().add(btnContact);
	}
}
