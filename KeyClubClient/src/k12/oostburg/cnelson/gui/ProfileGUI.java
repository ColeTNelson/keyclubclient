package k12.oostburg.cnelson.gui;

import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import k12.oostburg.cnelson.enums.Grade;
import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.enums.Role;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.Member;
import javax.swing.JSeparator;

public class ProfileGUI {

	private JFrame frame;
	private JButton btnDelete = new JButton("Delete");
	private JTextField tfUuid = new JTextField();
	private JTextField tfFName = new JTextField();
	private JTextField tfLName = new JTextField();
	private JTextField tfGrade = new JTextField();
	private JTextField tfRole = new JTextField();
	private JTextField tfEmail = new JTextField();
	private Member m;
	private Member self;

	/**
	 * Create the application.
	 */
	public ProfileGUI(Member m) {
		this.m = m;
		try {
			self = (Member) InputHandler.send(Request.SELF);
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}
		try {
			initialize();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.tfFName.setText(m.getFirstName());
		this.tfLName.setText(m.getLastName());
		this.tfGrade.setText(m.getGrade().toString());
		this.tfRole.setText(m.getRole().toString());
		this.tfEmail.setText(m.getEmail());
		this.tfUuid.setText(m.getUUID().toString());
		if(!self.isAdministrative()){
			btnDelete.setEnabled(false);
		}
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					InputHandler.send(Request.DELETE_MEMBER);
					InputHandler.send(m.getUUID());
				} catch (ClassNotFoundException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				frame.dispose();
			}
		});
		btnDelete.setBounds(12, 135, 97, 25);
		frame.getContentPane().add(btnDelete);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(120, 146, 295, 5);
		frame.getContentPane().add(separator);
		this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	private void initialize() throws ClassNotFoundException, IOException {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 221);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblProfile = new JLabel("Profile Overview");
		lblProfile.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblProfile.setBounds(12, 13, 171, 22);
		frame.getContentPane().add(lblProfile);
		
		JLabel lblUuid = new JLabel("UUID");
		lblUuid.setBounds(12, 48, 56, 16);
		frame.getContentPane().add(lblUuid);
		
		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(12, 77, 74, 16);
		frame.getContentPane().add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(12, 106, 74, 16);
		frame.getContentPane().add(lblLastName);
		
		JButton btnResetPassword = new JButton("Reset Password");
		btnResetPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new ResetPasswordGUI();
			}
		});
		btnResetPassword.setBounds(177, 14, 137, 25);
		frame.getContentPane().add(btnResetPassword);
		
		if(!self.isAdministrative()){
			tfFName.setEditable(false);
			tfLName.setEditable(false);
			tfGrade.setEditable(false);
			tfRole.setEditable(false);
		}
		
		tfUuid.setEditable(false);
		tfUuid.setBounds(89, 45, 137, 22);
		frame.getContentPane().add(tfUuid);
		tfUuid.setColumns(10);
		
		tfFName.setBounds(89, 74, 137, 22);
		frame.getContentPane().add(tfFName);
		tfFName.setColumns(10);
		
		tfLName.setBounds(89, 103, 137, 22);
		frame.getContentPane().add(tfLName);
		tfLName.setColumns(10);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(self.isAdministrative()){
						m.setFirstName(tfFName.getText());
						m.setLastName(tfLName.getText());
						Grade grade;
						try{
							grade = Grade.valueOfNumber(tfGrade.getText());
						}catch(Exception e){
							grade = Grade.valueOf(tfGrade.getText().toUpperCase());
						}
						m.setGrade(grade);
						m.setRole(Role.valueOf(tfRole.getText().toUpperCase()));
						m.setEmail(tfEmail.getText());
						InputHandler.send(Request.UPDATE_MEMBER);
						InputHandler.send(m);
					}else{
						InputHandler.send(Request.UPDATE_EMAIL_SELF);
						InputHandler.send(tfEmail.getText());
					}
				} catch (ClassNotFoundException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				frame.dispose();
			}
		});
		btnSubmit.setBounds(323, 14, 97, 25);
		frame.getContentPane().add(btnSubmit);
		
		JLabel lblGrade = new JLabel("Grade");
		lblGrade.setBounds(258, 48, 56, 16);
		frame.getContentPane().add(lblGrade);
		
		tfGrade.setBounds(304, 45, 116, 22);
		frame.getContentPane().add(tfGrade);
		tfGrade.setColumns(10);
		
		tfRole.setBounds(304, 74, 116, 22);
		frame.getContentPane().add(tfRole);
		tfRole.setColumns(10);
		
		JLabel lblRole = new JLabel("Role");
		lblRole.setBounds(258, 77, 34, 16);
		frame.getContentPane().add(lblRole);
		
		tfEmail.setBounds(304, 103, 116, 22);
		frame.getContentPane().add(tfEmail);
		tfEmail.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(258, 106, 40, 16);
		frame.getContentPane().add(lblEmail);
	}
}
