package k12.oostburg.cnelson.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import k12.oostburg.cnelson.enums.Request;
import k12.oostburg.cnelson.enums.ResponseCode;
import k12.oostburg.cnelson.handlers.InputHandler;
import k12.oostburg.cnelson.objects.Email;
import k12.oostburg.cnelson.objects.Member;

public class ReminderEmailGUI {

	private JLabel lblCheckPassword = new JLabel("Check Password");
	private JFrame frmKeyClub;
	private JTextField txtOostburgkeyclubgmailcom;
	private JSeparator separator;

	/**
	 * Create the application.
	 */
	public ReminderEmailGUI() {
		initialize();
		this.frmKeyClub.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKeyClub = new JFrame();
		frmKeyClub.setTitle("Key Club - Reminder Email");
		frmKeyClub.setBounds(100, 100, 450, 300);
		frmKeyClub.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmKeyClub.getContentPane().setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setToolTipText("Enter your message here...");
		textArea.setBounds(12, 79, 408, 161);
		frmKeyClub.getContentPane().add(textArea);
		
		txtOostburgkeyclubgmailcom = new JTextField();
		txtOostburgkeyclubgmailcom.setToolTipText("Enter your gmail here...");
		txtOostburgkeyclubgmailcom.setText("oostburgkeyclub@gmail.com");
		txtOostburgkeyclubgmailcom.setBounds(12, 44, 190, 23);
		frmKeyClub.getContentPane().add(txtOostburgkeyclubgmailcom);
		txtOostburgkeyclubgmailcom.setColumns(10);
		
		JButton btnSend = new JButton("Send");
		btnSend.setBounds(348, 43, 72, 25);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					ArrayList<Member> members = (ArrayList<Member>) InputHandler.send(Request.GET_MEMBERS);
					ArrayList<Email> emails = new ArrayList<Email>();
					ArrayList<String> text = new ArrayList<String>();
					text.add(textArea.getText());
					for(Member m : members){
						emails.add(new Email(m, "Key Club Notification", text));
					}
					InputHandler.send(Request.EMAIL);
					ResponseCode rc = (ResponseCode) InputHandler.send(emails);
					if(rc.equals(ResponseCode.UNDEFINED_FAILURE)){
						lblCheckPassword.setVisible(true);
						return;
					}
					frmKeyClub.dispose();
					JOptionPane.showMessageDialog(new JFrame(),
						    "Message sent!",
						    "Key Club",
						    JOptionPane.PLAIN_MESSAGE);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		frmKeyClub.getContentPane().add(btnSend);
		
		JLabel lblCustomReminderEmail = new JLabel("Key Club - Send Reminder E-Mail");
		lblCustomReminderEmail.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblCustomReminderEmail.setBounds(12, 7, 408, 23);
		frmKeyClub.getContentPane().add(lblCustomReminderEmail);
		
		separator = new JSeparator();
		separator.setBounds(12, 32, 408, 2);
		frmKeyClub.getContentPane().add(separator);
		
		lblCheckPassword = new JLabel("Check Password");
		lblCheckPassword.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCheckPassword.setForeground(Color.RED);
		lblCheckPassword.setBounds(220, 48, 110, 16);
		lblCheckPassword.setVisible(false);
		frmKeyClub.getContentPane().add(lblCheckPassword);
	}
}
