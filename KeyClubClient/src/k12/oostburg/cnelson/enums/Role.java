package k12.oostburg.cnelson.enums;

public enum Role {
	ADVISOR,
	OFFICER,
	MEMBER
}
