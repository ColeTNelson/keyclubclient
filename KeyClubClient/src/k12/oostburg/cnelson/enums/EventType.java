package k12.oostburg.cnelson.enums;

public enum EventType {
	ONGOING,
	CURRENT,
	ARCHIVED;
	
	public static EventType valueOfString(String s){
		if(s.equalsIgnoreCase("CURRENT")){
			return EventType.CURRENT;
		}else if(s.equalsIgnoreCase("ONGOING")){
			return EventType.ONGOING;
		}
		return EventType.ARCHIVED;
	}
}
