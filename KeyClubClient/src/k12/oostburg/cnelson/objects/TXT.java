package k12.oostburg.cnelson.objects;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class TXT {
	
	private ArrayList<String> content = new ArrayList<String>();
	
	public TXT(String fileName) throws IOException{
		load(new File(fileName));
	}
	
	public TXT(File file) throws IOException{
		load(file);
	}
	
	public void load(File file) throws IOException{
		BufferedReader in = new BufferedReader(new FileReader(file));
        String str;
        while((str = in.readLine()) != null){
            content.add(str);
        }
        in.close();
	}
	
	public ArrayList<String> generateForm(){
		return this.content;
	}
}
