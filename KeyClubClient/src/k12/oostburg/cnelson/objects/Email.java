package k12.oostburg.cnelson.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

public class Email implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7872278684676986034L;
	String address;
	String password;
	String subject;
	ArrayList<String> body;
	Member m;
	
	public Email(String address, String subject, ArrayList<String> body){
		this.address = address;
		this.subject = subject;
		this.body = body;
	}
	
	public Email(String address, String password, String subject, ArrayList<String> body){
		this.address = address;
		this.password = password;
		this.subject = subject;
		this.body = body;
	}
	
	public Email(Member m, String subject, ArrayList<String> body){
		this.m = m;
		this.address = m.getEmail();
		this.subject = subject;
		this.body = body;
	}
	
	public Email(Member m, String password, String subject, ArrayList<String> body){
		this.m = m;
		this.address = m.getEmail();
		this.password = password;
		this.subject = subject;
		this.body = body;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public ArrayList<String> getBody() {
		return body;
	}

	public void setBody(ArrayList<String> body) {
		this.body = body;
	}
}