package k12.oostburg.cnelson.objects;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.configuration.file.YamlConfiguration;

import k12.oostburg.cnelson.enums.EventType;

public class Event implements Serializable{
	UUID uuid;
	private static final long serialVersionUID = 4815223163680892362L;
	ArrayList<Member>  volunteers = new ArrayList<Member>();
	ArrayList<String> info = new ArrayList<String>();
	String address;
	double hours;
	int maxVolunteers;
	String name;
	String date;
	String time;
	boolean archived;
	
	public Event(UUID uuid, ArrayList<Member> volunteers, ArrayList<String> info, String address, String name, String date, String time, double hours, int maxVolunteers, boolean archived) {
		this.uuid = uuid;
		this.volunteers = volunteers;
		this.info = info;
		this.address = address;
		this.name = name;
		this.date = date;
		this.time = time;
		this.hours = hours;
		this.maxVolunteers = maxVolunteers;
		this.archived = archived;
	}
	
	public UUID getUUID() {
		return uuid;
	}
	public double getHours(){
		return hours;
	}
	public void setHours(double hours){
		this.hours = hours;
	}
	public ArrayList<Member> getVolunteers() {
		return volunteers;
	}
	public void setVolunteers(ArrayList<Member> volunteers) {
		this.volunteers = volunteers;
		ArrayList<String> strVolunteers = new ArrayList<String>();
		for(Member m : volunteers){
			strVolunteers.add(m.getUUID().toString());
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public ArrayList<String> getInfo() {
		return info;
	}
	public void setInfo(ArrayList<String> info) {
		this.info = info;
	}
	public int getMaxVolunteers() {
		return maxVolunteers;
	}

	public void setMaxVolunteers(int maxVolunteers) {
		this.maxVolunteers = maxVolunteers;
	}
	
	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public String toString(){
		return "Name:"+name+"\n"+"Date:"+date+"\n"+"Time:"+time;
	}
	
	public void setDetails(Event e) {
		this.address = e.getAddress();
		this.date = e.getDate();
		this.hours = e.getHours();
		this.info = e.getInfo();
		this.maxVolunteers = e.getMaxVolunteers();
		this.name = e.getName();
		this.time = e.getTime();
		this.volunteers = e.getVolunteers();
		this.uuid = e.getUUID();
		this.archived = e.isArchived();
	}

	public void addVolunteer(Member m) {
		removeVolunteer(m);
		this.volunteers.add(m);
	}
	
	public void removeVolunteer(Member m){
		for(Member mem : this.volunteers){
			if(mem.getUUID().equals(m.getUUID())){
				this.volunteers.remove(mem);
				return;
			}
		}
	}
}
